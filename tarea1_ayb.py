#Fecha
from datetime import datetime
def dia(fecha):
    semana = ('LUNES','MARTES','MIÉRCOLES','JUEVES','VIERNES','SÁBADO','DOMINGO')
    _fecha = datetime.strptime(fecha, "%d/%m/%Y %H:%M")
    print (semana[_fecha.weekday()])

def isLeapYear(fecha):
    _fecha = datetime.strptime(fecha, "%d/%m/%Y %H:%M")
    y=_fecha.year
    return y % 4 == 0 and (y % 100 != 0 or y % 400 == 0)


fecha="17/10/2000 12:09"
dia(fecha)
print (isLeapYear(fecha))